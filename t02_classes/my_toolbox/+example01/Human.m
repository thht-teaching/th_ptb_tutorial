classdef Human
  properties
    name
  end
  
  methods
    function hello(obj)
      disp('Hi!');
    end %function
    
    function say_name(obj)
      disp(['My name is ' obj.name '!']);
    end %function
  end
  
end

